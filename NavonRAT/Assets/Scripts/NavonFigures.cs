﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NavonFigure {

	public Sprite figure;
	public string localSymbol;
	public string globalSymbol;
	public int type;

	public NavonFigure(Sprite fig, string loc, string glo,int t){
		figure = fig;
		localSymbol = loc;
		globalSymbol = glo;
		type = t;
	}
}
