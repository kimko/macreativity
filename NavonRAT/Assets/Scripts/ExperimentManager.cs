﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExperimentManager {

	private static string participantID;

	[Range(0,2)] // 0 = not selected; 1 = pre test, 2 = post test
	private static int experimentPart = 0;
	private static string experimentType;

	public static List <int> repeatOptions = new List<int> ();

	public static bool navon1Finished;
	public static bool navon2Finished;
	public static bool rat1Finished;
	public static bool rat2Finished;


	public static string ParticipantID {
		get {
			return participantID;
		}
		set {
			participantID = value;
		}
	}

	public static int ExperimentPart {
		get {
			return experimentPart;
		}
		set {
			experimentPart = value;
		}
	}

	public static string ExperimentType {
		get {
			return experimentType;
		}
		set {
			experimentType = value;
		}
	}
}