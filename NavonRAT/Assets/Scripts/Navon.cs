﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Navon : MonoBehaviour {

	public class NavonFigure {

		public Sprite figure;
		public string localSymbol;
		public string globalSymbol;
		public int type; // 1 = consistent 2 = inconsistent 3 = neutral

		public NavonFigure(Sprite fig, string glo, string loc, int t){
			figure = fig;
			globalSymbol = glo;
			localSymbol = loc;
			type = t;
		}
	}

	private List<string[]> saveData = new List<string[]>();

	public float timeLimit;
	public float timer;
	public float waitingTime;
	public int mode;
	public bool identifyGlobal;
	public int currentFigure;
	public int figureCounter;
	public bool running, lastTaskWasGlobal, firstRound;

	public Image displayImage, bgPanel;
	public GameObject localText, globalText, exampleText, backToMenuButton;
	public AudioSource audioSource;

	public Sprite[] sprites = new Sprite[6];
	//public NavonFigure conH, incH, neuHG, neuHL, conS, incS, neuSG, neuSL;
	public NavonFigure xCon, xInc, oCon, oInc;
	public NavonFigure[] navonArray;
	public bool[] askForGlobal;

	// Use this for initialization
	void Start () {
		/*
		neuHL = new NavonFigure(sprites[0], "X", "H", 3);
		neuSL = new NavonFigure(sprites[1], "X", "S", 3);
		conH = new NavonFigure(sprites[2], "H", "H", 1);
		conS = new NavonFigure(sprites[3], "S", "S", 1);
		incH = new NavonFigure(sprites[4], "H", "S", 2);
		incS = new NavonFigure(sprites[5], "S", "H", 2);
		neuHG = new NavonFigure(sprites[6], "H", "X", 3);
		neuSG = new NavonFigure(sprites[7], "S", "X", 3);
		navonArray = new NavonFigure[8]{ neuHL, neuSL, conH, conS, incH, incS, neuHG, neuSG};
		*/

		xCon = new NavonFigure(sprites[0], "X", "X", 1);
		xInc = new NavonFigure(sprites[1], "X", "D", 2);
		oCon = new NavonFigure(sprites[2], "D", "D", 1);
		oInc = new NavonFigure(sprites[3], "D", "X", 2);
		navonArray = new NavonFigure[4]{xCon, xInc, oCon, oInc};


		Random.InitState (System.DateTime.Now.Millisecond);
		int randomConditionOrder = Random.Range (0, 2);
		switch (randomConditionOrder) {
		case 0:
			askForGlobal = new bool[20] { true, true, true, true, true, true, 
				false, false, false, false, false, false, 
				true, false, true, false, true, false, true, false
			};
			Debug.Log ("startig with global block");
			break;
		case 1:
			askForGlobal = new bool[20]{false, false, false, false, false, false, 
				true, true, true, true, true, true, 
				false, true, false, true, false, true, false, true
			};
			Debug.Log ("startig with local block");
			break;
		}
		// 6x Global, 6x Local, 8x L/G abwechselnd


		timeLimit = 5.0f;
		timer = timeLimit;
		waitingTime = 2f;
		//identifyGlobal = true;

		audioSource = GetComponent<AudioSource>();
		displayImage = GameObject.Find ("NavonFigure").GetComponent<Image> ();
		bgPanel = GameObject.Find ("Panel_BG").GetComponent<Image>();
		localText = GameObject.Find ("LocalTaskText");
		globalText = GameObject.Find ("GlobalTaskText");
		exampleText = GameObject.Find ("ExampleText");
		backToMenuButton = GameObject.Find ("BackToMenu_Button");
		localText.SetActive (false);
		globalText.SetActive (false);
		bgPanel.gameObject.SetActive (false);
		backToMenuButton.SetActive (false);

		// making the header (first column) of the save file
		string[] saveDataTemp = new string[7];
		saveDataTemp[0] = "Trial No.";
		saveDataTemp[1] = "Navon Figure";
		saveDataTemp[2] = "Global Condition?";
		saveDataTemp[3] = "Global/Local/Type";
		saveDataTemp[4] = "Answer";
		saveDataTemp[5] = "Correct";
		saveDataTemp[6] = "Reaction Time";
		saveData.Add(saveDataTemp);

		running = true;
		figureCounter = 0;
		currentFigure = 0;
		mode = 0;
		firstRound = true;
		identifyGlobal = askForGlobal [figureCounter];

	}
	
	// Update is called once per frame
	void Update () {

		if (running) {
			switch (mode) {
			case 0:
				if (Input.GetKeyDown (KeyCode.Return)) {
					exampleText.SetActive (false);
					mode = 2;
					StartCoroutine ("waitBetweenFigures");
					Random.InitState (System.DateTime.Now.Millisecond);
					currentFigure = Random.Range (0, navonArray.Length);
					displayImage.sprite = navonArray [currentFigure].figure;
				}
				break;
			case 1: 
				timer -= Time.deltaTime;

				if (Input.GetButtonDown("Kreuz")) {
					registerAnswer ("X");
					showNextFigure ();
				}
				if (Input.GetButtonDown("Dreieck")) {
					registerAnswer ("D");
					showNextFigure ();
				}
				if (timer < 0) {
					saveFigureData ("NONE", false);
					showNextFigure ();
				}
				break;
			case 2:
				if ((lastTaskWasGlobal != identifyGlobal) || firstRound) {
					bgPanel.gameObject.SetActive (true);
					if(identifyGlobal){
						bgPanel.color = new Color32 (0, 125, 0,255);
						globalText.SetActive (true);
					} else {
						bgPanel.color = new Color32 (125,0,0,255);
						localText.SetActive (true);
					}
					firstRound = false;
				}
				break;
			}
		} else {
			bgPanel.gameObject.SetActive (true);
			backToMenuButton.SetActive (true);
		}

		GetComponent<SaveToCSV>().saveToCSV (saveData);
	}

	void registerAnswer(string answer){
		if (identifyGlobal) {
			if (answer == navonArray[currentFigure].globalSymbol) {
				Debug.Log ("CORRECT bc global " + navonArray [currentFigure].globalSymbol + " == " + answer);
				saveFigureData(answer, true);

			} else {
				Debug.Log ("INCORRECT bc global " + navonArray [currentFigure].globalSymbol + " != " + answer);
				saveFigureData(answer, false);
			}
		} else {
			if (answer == navonArray[currentFigure].localSymbol) {
				Debug.Log ("CORRECT bc local " + navonArray [currentFigure].localSymbol + " == " + answer);
				saveFigureData(answer, true);

			} else {
				Debug.Log ("INCORRECT bc local " + navonArray [currentFigure].localSymbol + " != " + answer);
				saveFigureData(answer, false);
			}
		}
	}

	void saveFigureData(string answer, bool answerCorrect){
		string[] saveDataTemp = new string[7];
		saveDataTemp[0] = figureCounter.ToString(); 
		saveDataTemp[1] = currentFigure.ToString(); 
		saveDataTemp[2] = identifyGlobal.ToString(); 
		saveDataTemp[3] = navonArray[currentFigure].globalSymbol.ToString() +  "/" 
			+ navonArray[currentFigure].localSymbol.ToString() + "/" + navonArray[currentFigure].type.ToString(); 
		saveDataTemp[4] = answer;
		saveDataTemp[5] = answerCorrect.ToString();
		saveDataTemp[6] = (timeLimit - timer).ToString(); 
		saveData.Add(saveDataTemp);
	}

	void showNextFigure(){

			figureCounter++;
			if (figureCounter < askForGlobal.Length) {
				lastTaskWasGlobal = identifyGlobal; 
				identifyGlobal = askForGlobal [figureCounter];

				Random.InitState (System.DateTime.Now.Millisecond);
				currentFigure = Random.Range (0, navonArray.Length);

				if (identifyGlobal) {
					displayImage.sprite = navonArray [currentFigure].figure;
					Debug.Log ("randomly selected figure: " + currentFigure + " for global eval");

				} else {
					displayImage.sprite = navonArray [currentFigure].figure;
					Debug.Log ("randomly selected figure: " + currentFigure + " for local eval");
				}

				mode = 2;
				StartCoroutine ("waitBetweenFigures");
			} else {
				running = false;
			}

	}

	IEnumerator waitBetweenFigures(){
		//Debug.Log ("start PAUSE");
		bgPanel.gameObject.SetActive (true);
		yield return new WaitForSeconds(waitingTime);
		//Debug.Log ("done PAUSE");
		bgPanel.color = Color.black;
		bgPanel.gameObject.SetActive (false);
		localText.SetActive (false);
		globalText.SetActive (false);
		mode = 1;
		timer = timeLimit;
		audioSource.Play();
	}
	
}
