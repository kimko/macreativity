﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class ButtonClick : MonoBehaviour {

	public void startRAT(int testPart){
		ExperimentManager.ExperimentPart = testPart;
		ExperimentManager.ExperimentType = "RAT";
		SceneManager.LoadScene (1);
		Debug.Log ("STARTING rat part " + ExperimentManager.ExperimentPart);
		switch (testPart) {
		case 1:
			ExperimentManager.rat1Finished = true;
			break;
		case 2:
			ExperimentManager.rat2Finished = true;
			break;
		}
	}

	public void startNavon(int testPart){
		ExperimentManager.ExperimentPart = testPart;
		ExperimentManager.ExperimentType = "Navon";
		SceneManager.LoadScene (2);
		Debug.Log ("STARTING navon part " + ExperimentManager.ExperimentPart);
		switch (testPart) {
		case 1:
			ExperimentManager.navon1Finished = true;
			break;
		case 2:
			ExperimentManager.navon2Finished = true;
			break;
		}
	}

	public void backToMenu(){
		SceneManager.LoadScene (0);
	}
}
