﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class ID : MonoBehaviour {

	void Start ()
	{
		InputField input = gameObject.GetComponent<InputField>();
		var se = new InputField.SubmitEvent();
		se.AddListener(SubmitName);
		input.onEndEdit = se;
		
		if (ExperimentManager.navon1Finished) {
			input.text = ExperimentManager.ParticipantID; // for convienence
			GameObject.Find ("NavonPreButton").GetComponent<Image> ().color = Color.green;
		}
		if (ExperimentManager.navon2Finished) {
			input.text = ExperimentManager.ParticipantID; // for convienence
			GameObject.Find ("NavonPostButton").GetComponent<Image> ().color = Color.green;
		}
		if (ExperimentManager.rat1Finished) {
			input.text = ExperimentManager.ParticipantID; // for convienence
			GameObject.Find ("RATPreButton").GetComponent<Image> ().color = Color.green;
		}
		if (ExperimentManager.rat2Finished) {
			input.text = ExperimentManager.ParticipantID; // for convienence
			GameObject.Find ("RATPostButton").GetComponent<Image> ().color = Color.green;
		}

	}

	private void SubmitName(string inputText)
	{
		ExperimentManager.ParticipantID = inputText;
		Debug.Log("Participant ID is: " + inputText);
	}
		
}