﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;
using System.IO;

// DIE RAT SZENE MUSS ÜBER DIE ID SZENE GESTARTET WERDEN SONST GEHTS NICHT

public class RAT : MonoBehaviour {

	private List<string[]> saveData = new List<string[]>();

	public float timeLimit;
	public float timer;
	public int mode; // 0 = not started, 1 = timer runs, 2 = waiting for confirmation, 3 = pause
	public bool running;
	public float waitingTime;

	int currentPair;

	public Text word1;
	public Text word2;
	public Text word3;

	public Image blackPanel, whitePanel;
	public Text beispielText;
	public GameObject backToMenuButton;
	public AudioSource audioSource;

	public string[,] example = new string[2, 3];
	public string[,] rat = new string[9, 3]; // filled depending on exp part
	public string[,] preRat = new string[9, 3];
	public string[,] postRat = new string[9, 3];
	public List <int> selectedRepeatOptions = new List<int> ();
	public string[,] postRatIncludingRepeat;


	public bool[] answers;
	public float[] reactionTime;

	// Use this for initialization
	void Start () {
		//THESE VARIABLES CAN BE CHANGED
		timeLimit = 20f;
		waitingTime = 2f;

		//THESE VARIABLES SHOULDNT BE CHANGED
		running = true;
		currentPair = 0;
		timer = timeLimit;
		mode = 0;
			
		word1 = GameObject.Find ("Word1").GetComponent<Text>();
		word2 = GameObject.Find ("Word2").GetComponent<Text>();
		word3 = GameObject.Find ("Word3").GetComponent<Text>();

		audioSource = GetComponent<AudioSource>();
		blackPanel = GameObject.Find ("Panel_Black").GetComponent<Image>();
		whitePanel = GameObject.Find ("Panel_White").GetComponent<Image>();
		blackPanel.gameObject.SetActive (false);
		beispielText = GameObject.Find ("Beispieltext").GetComponent<Text>();
		backToMenuButton = GameObject.Find ("BackToMenu_Button");
		backToMenuButton.SetActive (false);

		// making the header (first column) of the save file
		string[] saveDataTemp = new string[4];
		saveDataTemp[0] = "Words";
		saveDataTemp[1] = "Pair No";
		saveDataTemp[2] = "Correct";
		saveDataTemp[3] = "Reaction Time";
		saveData.Add(saveDataTemp);

		/*
		//EXAMPLE
		example[0,0] = "Maß";
		example[0,1] = "Wurm";
		example[0,2] = "Video";

		example[1,0] = "Maß-band";
		example[1,1] = "Band-wurm";
		example[1,2] = "Video-band";

		//PRE RAT PAIRS
		preRat[0,0] = "Fisch";
		preRat[0,1] = "Mine";
		preRat[0,2] = "Rausch";

		preRat[1,0] = "Feuer";
		preRat[1,1] = "Sommer";
		preRat[1,2] = "Platz";

		preRat[2,0] = "Stopp";
		preRat[2,1] = "Tasche";
		preRat[2,2] = "Turm";

		preRat[3,0] = "Kontakt";
		preRat[3,1] = "Blick";
		preRat[3,2] = "Apfel";

		preRat[4,0] = "Zahn";
		preRat[4,1] = "Haare";
		preRat[4,2] = "Schuh";

		preRat[5,0] = "Note";
		preRat[5,1] = "Bund";
		preRat[5,2] = "General";

		preRat[6,0] = "Jahr";
		preRat[6,1] = "Stück";
		preRat[6,2] = "Schicht";

		preRat[7,0] = "Land";
		preRat[7,1] = "Milch";
		preRat[7,2] = "Haus";

		preRat[8,0] = "Auto";
		preRat[8,1] = "Route";
		preRat[8,2] = "Gefahr";

		//POST RAT PAIRS
		postRat[0,0] = "Park";
		postRat[0,1] = "Note";
		postRat[0,2] = "Konto";

		postRat[1,0] = "Nummer";
		postRat[1,1] = "Anruf";
		postRat[1,2] = "Buch";

		postRat[2,0] = "Wasser";
		postRat[2,1] = "Mine";
		postRat[2,2] = "Streuer";

		postRat[3,0] = "Zeit";
		postRat[3,1] = "Kapsel";
		postRat[3,2] = "Schiff";

		postRat[4,0] = "Start";
		postRat[4,1] = "Alarm";
		postRat[4,2] = "Schuss";

		postRat[5,0] = "Party";
		postRat[5,1] = "Tuch";
		postRat[5,2] = "Bad";

		postRat[6,0] = "Weg";
		postRat[6,1] = "Zeile";
		postRat[6,2] = "Bar";

		postRat[7,0] = "Bad";
		postRat[7,1] = "Blume";
		postRat[7,2] = "Uhr";

		postRat[8,0] = "Album";
		postRat[8,1] = "Name";
		postRat[8,2] = "Pflege";

		*/

		//ENGLISH EXAMPLE
		example[0,0] = "aid";
		example[0,1] = "rubber";
		example[0,2] = "wagon";

		example[1,0] = "band-aid";
		example[1,1] = "rubber-band";
		example[1,2] = "band-wagon";

		//ENGLISH PRE RAT PAIRS
		preRat[0,0] = "cream";
		preRat[0,1] = "skate";
		preRat[0,2] = "water";

		preRat[1,0] = "show";
		preRat[1,1] = "life";
		preRat[1,2] = "row";

		preRat[2,0] = "dew";
		preRat[2,1] = "comb";
		preRat[2,2] = "bee";

		preRat[3,0] = "cracker";
		preRat[3,1] = "fly";
		preRat[3,2] = "fighter";

		preRat[4,0] = "dream";
		preRat[4,1] = "break";
		preRat[4,2] = "light";

		preRat[5,0] = "fish";
		preRat[5,1] = "mine";
		preRat[5,2] = "rush";

		preRat[6,0] = "basket";
		preRat[6,1] = "eight";
		preRat[6,2] = "snow";

		preRat[7,0] = "wheel";
		preRat[7,1] = "hand";
		preRat[7,2] = "shopping";

		preRat[8,0] = "cross";
		preRat[8,1] = "rain";
		preRat[8,2] = "tie";

		//ENGLISH POST RAT PAIRS
		postRat[0,0] = "cottage";
		postRat[0,1] = "swiss";
		postRat[0,2] = "cake";

		postRat[1,0] = "loser";
		postRat[1,1] = "throat";
		postRat[1,2] = "spot";

		postRat[2,0] = "night";
		postRat[2,1] = "wrist";
		postRat[2,2] = "stop";

		postRat[3,0] = "safety";
		postRat[3,1] = "cushion";
		postRat[3,2] = "point";

		postRat[4,0] = "cane";
		postRat[4,1] = "daddy";
		postRat[4,2] = "plum";

		postRat[5,0] = "political";
		postRat[5,1] = "surprise";
		postRat[5,2] = "line";

		postRat[6,0] = "right";
		postRat[6,1] = "cat";
		postRat[6,2] = "carbon";

		postRat[7,0] = "home";
		postRat[7,1] = "sea";
		postRat[7,2] = "bed";

		postRat[8,0] = "nuclear";
		postRat[8,1] = "feud";
		postRat[8,2] = "album";


		// check if pre or post test
		switch(ExperimentManager.ExperimentPart){
		case 1:
			// PRE TEST: just use the normal pre-test array (9 items)
			rat = preRat;
			answers = new bool[9];
			reactionTime = new float[9];
			break;
		case 2:
			// POST TEST: first check for false answers in pre test, then add up to 3 of those to post test array
			// meaning: post array can be from 9 to 12 items long
			getRepeatOptions ();
			Debug.Log ("selectedRepeatOptions is this long: " + selectedRepeatOptions.Count);
			postRatIncludingRepeat = new string [(9 + selectedRepeatOptions.Count), 3];
			addRepeatOptions ();
			rat = postRatIncludingRepeat;
			answers = new bool[9+selectedRepeatOptions.Count];
			reactionTime = new float[9+selectedRepeatOptions.Count];
			break;
		}

		// show example
		word1.text = example[0,0];
		word2.text = example[0,1];
		word3.text = example[0,2];

	}
	
	// Update is called once per frame
	void Update () {

		if (running) {
			switch (mode) {
			//HASNT STARTED YET
			case 0: 
				//backToMenuButton.SetActive (true);
				if (Input.GetKeyDown (KeyCode.Space)) {
					word1.text = example[1,0];
					word2.text = example[1,1];
					word3.text = example[1,2];

				}
				if (Input.GetKeyDown (KeyCode.Return)) {
					mode = 3;
					StartCoroutine ("waitBetweenPairs");
					beispielText.gameObject.SetActive (false);
					word1.text = rat[0,0];
					word2.text = rat[0,1];
					word3.text = rat[0,2];
				}
				break;
			//TIMER IS RUNNING
			case 1: 
				timer -= Time.deltaTime;

				if (Input.GetButtonDown("Kreuz")) {
					mode = 2;
				}

				if (timer < 0) {
					if (ExperimentManager.ExperimentPart == 1) {
						ExperimentManager.repeatOptions.Add (currentPair);
					}
					answers [currentPair] = false;
					savePairData ();
					showNextPair ();
				}
				break;
			//TIMER STOP, WAITING FOR INPUT
			case 2: 
				Debug.Log("WAITING FOR INPUT");
				whitePanel.color = new Color32 (160, 160, 160, 255);
				if (Input.GetKeyDown (KeyCode.R)) {
					registerAnswer (true);

				} else if (Input.GetKeyDown (KeyCode.W)) {
					if (ExperimentManager.ExperimentPart == 1) {
						ExperimentManager.repeatOptions.Add (currentPair);
					}
					registerAnswer (false);
				}
				break;
			//TIMER STOP, PAUSE BETWEEN PAIRS
			case 3: 
				// hier darf man nüschts machen
				break;
			}
		} else {
			blackPanel.gameObject.SetActive (true);
			backToMenuButton.SetActive (true);
		}

		// continously save data in file
		GetComponent<SaveToCSV>().saveToCSV (saveData);
	}

	// show the next pair from the rat array if possible
	void showNextPair(){
		if (currentPair < ((rat.Length/3)-1)) {
			Debug.Log ("going to next pair");
			currentPair += 1;
			word1.text = rat [currentPair, 0];
			word2.text = rat [currentPair, 1];
			word3.text = rat [currentPair, 2];
			mode = 3;
			StartCoroutine ("waitBetweenPairs");
		} else {
			running = false;
		}

	}

	// after each pair (doesnt matter if it was answered or not) there is a pause until the next appears
	IEnumerator waitBetweenPairs(){
		Debug.Log ("start PAUSE");
		blackPanel.gameObject.SetActive (true);
		yield return new WaitForSeconds(waitingTime);
		Debug.Log ("done PAUSE");
		blackPanel.gameObject.SetActive (false);
		mode = 1;
		timer = timeLimit;
		audioSource.Play();
	}

	// after space + R/W are pressed, the answer is recorded
	void registerAnswer(bool isAnswerCorrect){
		answers[currentPair] = isAnswerCorrect;
		reactionTime [currentPair] = timeLimit - timer;
		savePairData ();
		showNextPair ();
		whitePanel.color = Color.white;
	}

	// saves last answer to csv file
	void savePairData(){
		string[] saveDataTemp = new string[4];
		saveDataTemp[0] = rat[currentPair,0] + "/" + rat[currentPair,1] + "/" + rat[currentPair,2]; 
		saveDataTemp[1] = currentPair.ToString(); 
		saveDataTemp[2] = answers[currentPair].ToString();
		saveDataTemp[3] = reactionTime[currentPair].ToString(); 
		saveData.Add(saveDataTemp);
	}

	// from all the wrong answers of the pre-rat, select a maximum of 3
	void getRepeatOptions(){
		Debug.Log ("NO OF REPEAT OPTIONS: " + ExperimentManager.repeatOptions.Count);
		if (ExperimentManager.repeatOptions.Count > 0) {
			int firstOption = Random.Range (0, ExperimentManager.repeatOptions.Count - 1);
			selectedRepeatOptions.Add (ExperimentManager.repeatOptions [firstOption]);
			ExperimentManager.repeatOptions.RemoveAt (firstOption);
			Debug.Log ("SELECTED: " + selectedRepeatOptions [0]);
			if (ExperimentManager.repeatOptions.Count > 0) {
				int secondOption = Random.Range (0, ExperimentManager.repeatOptions.Count-1);
				selectedRepeatOptions.Add (ExperimentManager.repeatOptions[secondOption]);
				ExperimentManager.repeatOptions.RemoveAt (secondOption);
				Debug.Log ("SELECTED: " + selectedRepeatOptions [1]);
				if (ExperimentManager.repeatOptions.Count > 0) {
					int thirdOption = Random.Range (0, ExperimentManager.repeatOptions.Count - 1);
					selectedRepeatOptions.Add (ExperimentManager.repeatOptions [thirdOption]);
					ExperimentManager.repeatOptions.RemoveAt (thirdOption);
					Debug.Log ("SELECTED: " + selectedRepeatOptions [2]);
				}
			}
		}
	}

	// add the selected wrong answers (max 3) from the pre-array to the post-array
	void addRepeatOptions(){
		for (int i = 0; i < (9 + selectedRepeatOptions.Count); i++) {
			if (i < 9) {
				Debug.Log ("at item no " + i + " of normal postrat");
				postRatIncludingRepeat [i, 0] = postRat [i, 0];
				postRatIncludingRepeat [i, 1] = postRat [i, 1];
				postRatIncludingRepeat [i, 2] = postRat [i, 2];
			} else if (i >= 9) {
				Debug.Log ("at item no " + i + " of extra postrat, adding " + selectedRepeatOptions[i-9]);
				postRatIncludingRepeat [i, 0] = preRat [selectedRepeatOptions[i-9], 0];
				postRatIncludingRepeat [i, 1] = preRat [selectedRepeatOptions[i-9], 1];
				postRatIncludingRepeat [i, 2] = preRat [selectedRepeatOptions[i-9], 2];
			}
		}
	}
		


	

}
