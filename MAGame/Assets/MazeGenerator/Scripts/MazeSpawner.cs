﻿using UnityEngine;
using System.Collections;

//<summary>
//Game object, that creates maze and instantiates it in scene
//</summary>
public class MazeSpawner : MonoBehaviour {
	public enum MazeGenerationAlgorithm{
		PureRecursive,
		RecursiveTree,
		RandomTree,
		OldestTree,
		RecursiveDivision,
	}

	public MazeGenerationAlgorithm Algorithm = MazeGenerationAlgorithm.PureRecursive;
	public bool FullRandom = false;
	public int RandomSeed = 12345;
	public GameObject Floor = null;
	public GameObject WallPrefab = null;
	public GameObject Pillar = null;
	public int Rows = 5;
	public int Columns = 5;
	public float CellWidth = 5;
	public float CellHeight = 5;
	public bool AddGaps = true;
	public GameObject GoalPrefab = null;

	//von kim geaddet
	public GameObject WallAlternative = null;
	public GameObject WallAlternative2 = null;
	public GameObject Wall = null;
	public GameObject Shortcut = null;
	public int NumberOfShortcuts = 2;
	bool[,] myArray; 
	public bool mitShortcuts = false;

	private BasicMazeGenerator mMazeGenerator = null;

	void Start () {
			myArray = new bool[Rows, Columns];
			for (int i = 0; i < Rows; i++) {
				for (int j = 0; j < Columns; j++) {
					myArray [i, j] = false;
				}
			}

			if (Shortcut != null) {
				for (int i = 0; i < NumberOfShortcuts; i++) {
					Random.seed = System.DateTime.Now.Millisecond;
					int randomRow = Random.Range (0, Rows);
					int randomCol = Random.Range (0, Columns);
					myArray [randomRow, randomCol] = true;
					print (randomRow + "," + randomCol);
				}
			}

		if (!FullRandom) {
			Random.seed = RandomSeed;
		}
		switch (Algorithm) {
		case MazeGenerationAlgorithm.PureRecursive:
			mMazeGenerator = new RecursiveMazeGenerator (Rows, Columns);
			break;
		case MazeGenerationAlgorithm.RecursiveTree:
			mMazeGenerator = new RecursiveTreeMazeGenerator (Rows, Columns);
			break;
		case MazeGenerationAlgorithm.RandomTree:
			mMazeGenerator = new RandomTreeMazeGenerator (Rows, Columns);
			break;
		case MazeGenerationAlgorithm.OldestTree:
			mMazeGenerator = new OldestTreeMazeGenerator (Rows, Columns);
			break;
		case MazeGenerationAlgorithm.RecursiveDivision:
			mMazeGenerator = new DivisionMazeGenerator (Rows, Columns);
			break;
		}
		mMazeGenerator.GenerateMaze ();
		for (int row = 0; row < Rows; row++) {
			for(int column = 0; column < Columns; column++){
				float x = column*(CellWidth+(AddGaps?2.0f:0));
				float z = row*(CellHeight+(AddGaps?2.0f:0));
				MazeCell cell = mMazeGenerator.GetMazeCell(row,column);
				GameObject tmp;
				tmp = Instantiate(Floor,new Vector3(x,0,z), Quaternion.Euler(0,0,0)) as GameObject;
				tmp.transform.parent = transform;

				//IF WALL ALTERNATIVES ARE AVAILABLE, RANDOMLY CHOOSE WHICH WALL WILL BE PUT DOWN
				if (WallAlternative != null && WallAlternative2 != null) {
					Debug.Log("first if");
					Random.seed = System.DateTime.Now.Millisecond;
					int randomWallNo = Random.Range (0, 3);
					Debug.Log(randomWallNo);
					if (randomWallNo == 0) {
						Wall = WallPrefab;
					} else if (randomWallNo == 1) {
						Wall = WallAlternative;
					} else if (randomWallNo == 2) {
						Wall = WallAlternative2;
					}
				}
				else if (WallAlternative != null && WallAlternative2 == null) {
					Debug.Log("second if");
					Random.seed = System.DateTime.Now.Millisecond;
					int randomWallNo = Random.Range (0, 2);
					Debug.Log(randomWallNo);
					if (randomWallNo == 0) {
						Wall = WallPrefab;
					} else if (randomWallNo == 1) {
						Wall = WallAlternative;
					}
				} else {
					Debug.Log("else");
					Wall = WallPrefab;
				}
				

				if (row < Rows - 1) {
					if (cell.WallFront && !(mMazeGenerator.GetMazeCell (row + 1, column).WallBack)) {
						if(myArray[row,column] == true && mitShortcuts == true){
							tmp = Instantiate (Shortcut, new Vector3 (x, 0, z + CellHeight / 2) + Shortcut.transform.position, Quaternion.Euler (0, 0, 0)) as GameObject;// front
							tmp.transform.parent = transform;
						}
						else {
							tmp = Instantiate (Wall, new Vector3 (x, 0, z + CellHeight / 2) + Wall.transform.position, Quaternion.Euler (0, 0, 0)) as GameObject;// front
							tmp.transform.parent = transform;
						}
					}
				} else {
					if (cell.WallFront) {
							tmp = Instantiate (Wall, new Vector3 (x, 0, z + CellHeight / 2) + Wall.transform.position, Quaternion.Euler (0, 0, 0)) as GameObject;// front
							tmp.transform.parent = transform;
					}
				}
				if (column < Columns - 1) {
					if (cell.WallRight && !(mMazeGenerator.GetMazeCell (row, column + 1).WallLeft)) { 
						if(myArray[row,column] == true && mitShortcuts == true){
							tmp = Instantiate (Shortcut, new Vector3 (x, 0, z + CellHeight / 2) + Shortcut.transform.position, Quaternion.Euler (0, 0, 0)) as GameObject;// front
							tmp.transform.parent = transform;
						}
						else {
							tmp = Instantiate (Wall, new Vector3 (x + CellWidth / 2, 0, z) + Wall.transform.position, Quaternion.Euler (0, 90, 0)) as GameObject;// right
							tmp.transform.parent = transform;
						}
					}
				} else {
					if (cell.WallRight) { 
						tmp = Instantiate (Wall, new Vector3 (x + CellWidth / 2, 0, z) + Wall.transform.position, Quaternion.Euler (0, 90, 0)) as GameObject;// right
						tmp.transform.parent = transform;
					}
				}
					if(cell.WallLeft){
						if(myArray[row,column] == true && mitShortcuts == true){
							tmp = Instantiate (Shortcut, new Vector3 (x, 0, z + CellHeight / 2) + Shortcut.transform.position, Quaternion.Euler (0, 0, 0)) as GameObject;// front
							tmp.transform.parent = transform;
						}
						else {
							tmp = Instantiate(Wall,new Vector3(x-CellWidth/2,0,z)+Wall.transform.position,Quaternion.Euler(0,270,0)) as GameObject;// left
							tmp.transform.parent = transform;
						}
					}
					if(cell.WallBack){
						if(myArray[row,column] == true && mitShortcuts == true){
							tmp = Instantiate (Shortcut, new Vector3 (x, 0, z + CellHeight / 2) + Shortcut.transform.position, Quaternion.Euler (0, 0, 0)) as GameObject;// front
							tmp.transform.parent = transform;
						}
						else {
							tmp = Instantiate(Wall,new Vector3(x,0,z-CellHeight/2)+Wall.transform.position,Quaternion.Euler(0,180,0)) as GameObject;// back
							tmp.transform.parent = transform;
						}
					}
					if(cell.IsGoal && GoalPrefab != null){
						tmp = Instantiate(GoalPrefab,new Vector3(x,1,z), Quaternion.Euler(0,0,0)) as GameObject;
						tmp.transform.parent = transform;
					}

			}
		}
		if(Pillar != null){
			for (int row = 0; row < Rows+1; row++) {
				for (int column = 0; column < Columns+1; column++) {
					float x = column*(CellWidth+(AddGaps?2f:0));
					float z = row*(CellHeight+(AddGaps?2f:0));
					GameObject tmp = Instantiate(Pillar,new Vector3(x-CellWidth/2,0,z-CellHeight/2),Quaternion.identity) as GameObject;
					tmp.transform.parent = transform;
				}
			}
		}
	}
}
