﻿using UnityEngine.Audio;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AudioManager : MonoBehaviour {

	public static AudioManager instance;

	public AudioMixerGroup mixerGroup;

	public Sound[] sounds;

	DataManager dataMan;

	void Awake() {
		if (instance != null)
		{
			Destroy(gameObject);
		}
		else
		{
			instance = this;
			DontDestroyOnLoad(gameObject);
		}

		foreach (Sound s in sounds)
		{
			s.source = gameObject.AddComponent<AudioSource>();
			s.source.clip = s.clip;
			s.source.loop = s.loop;

			s.source.outputAudioMixerGroup = mixerGroup;
		}


	}

	void Start(){
		dataMan = FindObjectOfType<DataManager> ();
	}

	void Update(){

		if ((dataMan.currentSceneName == "StartScreen" && !isPlaying("start_BGM"))
			|| (dataMan.currentSceneName == "LevelScreen" && !isPlaying("start_BGM"))
			|| (dataMan.currentSceneName == "DeathScreen" && !isPlaying("start_BGM"))) {
			Stop ("level_BGM");
			Play ("start_BGM");
		}
		else if (dataMan.currentSceneIndex >= 3 && dataMan.currentSceneIndex <= 15 && !isPlaying("level_BGM")) {
			Stop ("start_BGM");
			Play ("level_BGM");
		}
		else if (dataMan.currentSceneName == "WinScreenFinal" && !isPlaying("victory_BGM")) {
			Stop ("level_BGM");
			Stop ("start_BGM");
			Play ("victory_BGM");
		}
	}

	public void Play(string sound)
	{
		Sound s = Array.Find(sounds, item => item.name == sound);
		if (s == null)
		{
			Debug.LogWarning("Sound: " + name + " not found!");
			return;
		}

		s.source.volume = s.volume * (1f + UnityEngine.Random.Range(-s.volumeVariance / 2f, s.volumeVariance / 2f));
		s.source.pitch = s.pitch * (1f + UnityEngine.Random.Range(-s.pitchVariance / 2f, s.pitchVariance / 2f));

		s.source.Play();
	}

	public void Stop(string sound)
	{
		Sound s = Array.Find(sounds, item => item.name == sound);
		if (s == null)
		{
			Debug.LogWarning("Sound: " + name + " not found!");
			return;
		}

		s.source.Stop();
	}

	public bool isPlaying(string sound)
	{
		Sound s = Array.Find(sounds, item => item.name == sound);
		if (s == null)
		{
			Debug.LogWarning("Sound: " + name + " not found!");
			return false;
		}

		return s.source.isPlaying;
	}

}
