﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;
using System.IO;

public class SaveDataToCSV : MonoBehaviour {

	// transcribes the entire contents of the playerData list to the csv file
	public void saveToCSV(List<string[]> playerData){
		string[][] output = new string[playerData.Count][];

		for(int i = 0; i < output.Length; i++){
			output[i] = playerData[i];
		}

		int length = output.GetLength(0);
		string delimiter = ",";

		StringBuilder sb = new StringBuilder();

		for (int index = 0; index < length; index++)
			sb.AppendLine(string.Join(delimiter, output[index]));

		string filePath = getPath();

		StreamWriter outStream = System.IO.File.CreateText(filePath);
		outStream.WriteLine(sb);
		outStream.Close();
	}

	//	return Application.dataPath + "/CSV/" + ExperimentManager.ParticipantID + "_" + ExperimentManager.ExperimentType + "_part" + ExperimentManager.ExperimentPart + "_data.csv";


	private string getPath (){
		#if UNITY_EDITOR
		return Application.dataPath + "/CSV/" + FindObjectOfType<DataManager> ().participantID + "_Group" + FindObjectOfType<DataManager> ().experimentGroup.ToString() + "_data.csv";
		#elif UNITY_ANDROID
		return Application.persistentDataPath + "/" + FindObjectOfType<DataManager> ().participantID + "_Group" + FindObjectOfType<DataManager> ().experimentGroup.ToString() + "_data.csv";
		#elif UNITY_IPHONE
		return Application.persistentDataPath + "/" + FindObjectOfType<DataManager> ().participantID + "_Group" + FindObjectOfType<DataManager> ().experimentGroup.ToString() + "_data.csv";
		#else
		return Application.dataPath + "/" + FindObjectOfType<DataManager> ().participantID + "_Group" + FindObjectOfType<DataManager> ().experimentGroup.ToString() + "_data.csv";
		#endif
		}  
		}
