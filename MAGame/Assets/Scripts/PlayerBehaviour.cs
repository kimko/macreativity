﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerBehaviour : MonoBehaviour {

	public int lives = 3; 
	int collected = 0;
	int goal;
	public Vector3 startPosition;
	AudioManager audioMan;
	DataManager dataMan;
	GameObject heart1, heart2, heart3;
	int powerUpCount;
	bool invincible = false;
	float invincibilityDuration;
	public GameObject text, extraText;

	//Animation
	private Animator animator;

	// Use this for initialization
	void Start () {

		goal = GameObject.FindGameObjectsWithTag ("Objective").Length;
		invincibilityDuration = 3f;
		startPosition = transform.position;

		heart1 = GameObject.Find ("heart1");
		heart2 = GameObject.Find ("heart2");
		heart3 = GameObject.Find ("heart3");

		heart1.gameObject.SetActive (true);
		heart2.gameObject.SetActive (true);
		heart3.gameObject.SetActive (true);

		audioMan = FindObjectOfType<AudioManager> ();
		dataMan = FindObjectOfType<DataManager> ();
		animator = GameObject.Find("Fade").GetComponent<Animator> ();

		dataMan.playerStartingTimestampPerLevel [dataMan.currentSceneIndex] = System.DateTime.Now.ToString();
		dataMan.currentLevel = dataMan.currentSceneIndex - 2;
			
		//Debug.Log ("accessing " + dataMan.currentSceneIndex + " of playerStartingTimestampPerLevel, max: " + dataMan.playerStartingTimestampPerLevel.Length);


		if (dataMan.currentSceneIndex <=6 && dataMan.currentSceneIndex >=3){
			text = GameObject.Find ("text");
			StartCoroutine(TextEinblenden ());
			if (dataMan.currentSceneIndex == 4){
				extraText = GameObject.Find ("extraText");
				extraText.SetActive (false);
				if(dataMan.experimentGroup == 1) {
					extraText.SetActive (true);
				}
			}
		}
	}



	// Update is called once per frame
	void Update () {
		
		//checkForShortcuts ();
		if (lives > 3)
			lives = 3;
		switch (lives) {
		case 3:
			heart1.gameObject.SetActive (true);
			heart2.gameObject.SetActive (true);
			heart3.gameObject.SetActive (true);
			break;
		case 2:
			heart1.gameObject.SetActive (true);
			heart2.gameObject.SetActive (true);
			heart3.gameObject.SetActive (false);
			break;
		case 1:
			heart1.gameObject.SetActive (true);
			heart2.gameObject.SetActive (false);
			heart3.gameObject.SetActive (false);
			break;
		case 0:
			heart1.gameObject.SetActive (false);
			heart2.gameObject.SetActive (false);
			heart3.gameObject.SetActive (false);
			gameOver ();
			break;
		}

		if (Input.GetButton ("RestartToStart")) {
			SceneManager.LoadScene (0);
		}

	} 

	// Objectives and Enemies need to have a Collider with isTrigger activated and the correct Tag! Only the parent Object.
	void OnTriggerEnter(Collider other) {
		// collect treasures
		if (other.gameObject.CompareTag ("Objective")){
			other.gameObject.SetActive (false);
			collected++;
			audioMan.Play("treasureCollected");

			if (collected == goal) {
					levelComplete ();
				}

			}

		// take damage from enemies
		if (other.gameObject.CompareTag ("Enemy")){
			
			if (!invincible) {
				if (lives > 1) {
					dataMan.playerLivesLostPerLevel [dataMan.currentSceneIndex]++;
					Debug.Log ("You lost a life!");
					lives--;
					audioMan.Play ("enemyContact");
					StartCoroutine (FreezeFadeAndReposition ());
					StartCoroutine(TemporaryInvincibility());
					StartCoroutine (InvincibilityBlink (invincibilityDuration));
				} else {
					Debug.Log ("Lost all lives! Game Over!");
					audioMan.Play ("enemyContact");
					gameOver ();
				}
			} else {
				audioMan.Play("enemyBlocked");
			}
		}

		/*
		// collect powerups (golden invincibility chicks)
		if (other.gameObject.CompareTag ("PowerUp")){
			Destroy (other.gameObject);
			powerUpCount++;
			audioMan.Play("powerupCollected");

			if (powerUpCount == 3) {
				StartCoroutine(TemporaryInvincibility());
				powerUpCount = 0;
			}
		}
		*/
	}

	void levelComplete (){
		dataMan.playerFinishingTimestampPerLevel [dataMan.currentSceneIndex] = System.DateTime.Now.ToString();
		//dataMan.playerDurationPerLevel[dataMan.currentSceneIndex] = GetComponent<Timer> ().timerList[dataMan.currentSceneIndex] - GetComponent<Timer> ().timer;
		audioMan.Stop("countdown");
		audioMan.Play ("levelCompleted");
		if (dataMan.currentSceneIndex == 15) {
			SceneManager.LoadScene ("WinScreenFinal");
		} else {
			SceneManager.LoadScene ("WinScreen");
		}
	}

	public void gameOver(){
		dataMan.playerDeathsPerLevel [dataMan.currentSceneIndex]++;
		audioMan.Play ("gameOver");
		SceneManager.LoadScene ("DeathScreen");
	}

	IEnumerator InvincibilityBlink(float waitTime) {
		Debug.Log ("INVINCI BLINKING");
		float endTime = Time.time + waitTime;
		while(Time.time < endTime){
			GetComponentInChildren<Renderer>().enabled = false;
			yield return new WaitForSecondsRealtime (0.2f);
			GetComponentInChildren<Renderer>().enabled = true;
			yield return new WaitForSecondsRealtime (0.2f);
		}
	}

	// freeze game, fade to black and put player at starting position
	IEnumerator FreezeFadeAndReposition(){
		var original = Time.timeScale;
		Time.timeScale = 0f;
		animator.Play ("FadeIN");
		yield return new WaitForSecondsRealtime (0.5f);
		Time.timeScale = original;
		transform.position = startPosition;
	}

	IEnumerator TextEinblenden(){
		float textTime = 5f;
		switch (dataMan.currentSceneIndex) {
		case 3:
			textTime = 5f;
			break;
		case 4:
			textTime = 9f;
			break;
		case 5:
			textTime = 6f;
			break;
		case 6:
			textTime = 8f;
			break;
		}
		yield return new WaitForSecondsRealtime (textTime);
		text.SetActive (false);
		if (dataMan.currentSceneIndex == 4 && dataMan.experimentGroup == 1) {
			extraText.SetActive (false);
		}

	}

	IEnumerator TemporaryInvincibility(){
		invincible = true;
		//audioMan.Play("invincible_BGM");
		// TODO: eventuell player scheller machen?
		yield return new WaitForSecondsRealtime (invincibilityDuration);
		//audioMan.Stop("invincible_BGM");
		invincible = false;
	}
}
