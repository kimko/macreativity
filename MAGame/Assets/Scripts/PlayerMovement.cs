﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

	public float globalSpeed;
	public float localSpeed;
	public float rotationSensitivity; //wie schnell bewegt sich die kamera im local mode
	float xAxisClamp = 0.0f;

	float[] playerRestraints = new float[4];

	Rigidbody rb;
	Transform playerBody;
	DataManager dataMan;

	// Use this for initialization
	void Start () {
		dataMan = FindObjectOfType<DataManager> ();
		playerBody = GetComponent<Transform> ();
		rb = GetComponent<Rigidbody>();
		rb.freezeRotation = true;
		localSpeed = 3.5f;
		globalSpeed = 8.0f;
		rotationSensitivity = 2.0f;

		if (dataMan.currentSceneIndex == 4) {
			playerRestraints = new float[]{-0.2f, 12.2f, -0.2f, 12.2f};
		}
		else if (dataMan.currentSceneIndex == 5) {
			playerRestraints = new float[]{-0.2f, 16.2f, -0.2f, 16.2f};
		}
		else if (dataMan.currentSceneIndex == 6
			|| dataMan.currentSceneIndex == 10 || dataMan.currentSceneIndex == 13) {
			playerRestraints = new float[]{-0.2f, 20.2f, -0.2f, 20.2f};
		}
		else if (dataMan.currentSceneIndex == 7 || dataMan.currentSceneIndex == 8 
			|| dataMan.currentSceneIndex == 9 || dataMan.currentSceneIndex == 11 
			|| dataMan.currentSceneIndex == 12 || dataMan.currentSceneIndex == 14 || dataMan.currentSceneIndex == 15) {
			playerRestraints = new float[]{-0.2f, 32.2f, -0.2f, 20.2f};
		}
	}

	void Update(){
		// 
		if (!GetComponent<Switch> ().viewIsGlobal && Input.GetButtonDown("QuickTurn")){
			transform.rotation *= Quaternion.Euler(0, 180, 0);
			Debug.Log ("KEHRTWENDE");
		}
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		if(GetComponent<Switch>().viewIsGlobal){
			globalMovement ();
		}
		else if (!GetComponent<Switch> ().viewIsGlobal) {
			localCameraRotation ();
			localMovement ();
		}

		//if (!GetComponent<Switch> ().viewIsGlobal) {
			if (Input.GetButtonDown("Short")) {
				Debug.Log ("ATTEMPTING SHORTCUT");
				lookForShortcut ();
			}
		//}
		Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 2f, Color.yellow);
		Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 4f, Color.cyan);

	}

	void lookForShortcut(){
		Vector3 fwd = transform.TransformDirection(Vector3.forward);
		LayerMask shortcutMask = LayerMask.GetMask("Shortcut");
		LayerMask invisiMask = LayerMask.GetMask("InvisiWall");

		// if shortcut is directly in front of object, move trough shortcut wall
		if (Physics.Raycast (transform.position, fwd, 2f, shortcutMask.value)
			&& !(Physics.Raycast (transform.position, fwd, 4f, invisiMask.value))) {
			Debug.Log ("SHORTCUT: passed invisible wall check");

			Vector3 newPosition = transform.position + transform.forward * 2;
			if (newPosition.x > playerRestraints [0]
			   && newPosition.x < playerRestraints [1]
			   && newPosition.z > playerRestraints [2]
			   && newPosition.z < playerRestraints [3]) {

				dataMan.playerShortcutTimestampsPerLevel [dataMan.currentSceneIndex].Insert(dataMan.playerShortcutsPerLevel [dataMan.currentSceneIndex], System.DateTime.Now.ToString("HH:mm:ss"));
				dataMan.playerShortcutsPerLevel [dataMan.currentSceneIndex]++;

				transform.position += transform.forward * 2;
				FindObjectOfType<AudioManager> ().Play ("shortcut");
				Debug.Log ("SHORTCUT: passed constraint check == SUCCESSFULL");
			}
		}
	}

	void globalMovement(){
		float moveH = Input.GetAxisRaw ("Horizontal");
		float moveV = Input.GetAxisRaw ("Vertical");

		Vector3 movement = new Vector3 (moveH, 0.0f, moveV);
		if (movement != Vector3.zero)
			transform.rotation = Quaternion.Slerp (transform.rotation, Quaternion.LookRotation(movement.normalized), 0.2f);

		transform.Translate (movement * globalSpeed * Time.deltaTime, Space.World);
	}

	void localMovement(){
		var x = Input.GetAxis("Horizontal") * Time.deltaTime * localSpeed;
		var z = Input.GetAxis("Vertical") * Time.deltaTime * localSpeed;

		transform.Translate(x, 0, 0);
		transform.Translate(0, 0, z);
	}

	void localCameraRotation (){
		float mouseX = Input.GetAxis("Joy X");
		float mouseY = Input.GetAxis("Joy Y");

		float rotAmountX = mouseX * rotationSensitivity;
		float rotAmountY = mouseY * rotationSensitivity;

		xAxisClamp -= rotAmountY;

		Vector3 targetRotCam = transform.rotation.eulerAngles;
		Vector3 targetRotBody = playerBody.rotation.eulerAngles;

		targetRotCam.x -= rotAmountY;
		targetRotCam.z = 0;
		targetRotBody.y += rotAmountX;

		if(xAxisClamp > 90)
		{
			xAxisClamp = 90;
			targetRotCam.x = 90;
		}
		else if(xAxisClamp < -90)
		{
			xAxisClamp = -90;
			targetRotCam.x = 270;
		}

		transform.rotation = Quaternion.Euler(targetRotCam);
		playerBody.rotation = Quaternion.Euler(targetRotBody);
	}
		
}
