﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExtraFunctions : MonoBehaviour {

	DataManager dataMan;


	// Use this for initialization
	void Start () {
		dataMan = FindObjectOfType<DataManager> ();

	}

	// Update is called once per frame
	void Update () {
		if (dataMan.currentSceneIndex == 17) {
			if (Input.GetButtonDown ("QuickTurn")) {
				GetComponent<SceneOnKlick> ().loadNextLevel ();
			}
		}
		else if (dataMan.currentSceneIndex == 2){
			GetComponent<Text>().text  = dataMan.currentLevel.ToString ();
		}

	}
}
