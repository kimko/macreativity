﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class SceneOnKlick : MonoBehaviour {

	DataManager dataMan;
	bool deactivatedButton;

	void Start(){
		dataMan = FindObjectOfType<DataManager> ();
		deactivatedButton = false;

		if (dataMan.currentSceneIndex == 0) {
			InputField input = gameObject.GetComponentsInChildren<InputField> ()[0];
			var se = new InputField.SubmitEvent ();
			se.AddListener (SubmitName);
			input.onEndEdit = se;
		}
	}

	void Update(){
		if (dataMan.currentSceneIndex == 16) {
			if (dataMan.playerDeathsPerLevel [dataMan.previousSceneIndex] > 3 && !deactivatedButton) {
				GameObject.Find ("Retry").SetActive (false);
				deactivatedButton = true;
			}
			if (Input.GetButtonDown("QuickTurn") && dataMan.playerDeathsPerLevel[dataMan.previousSceneIndex] <= 3){
				restartPreviousLevel ();
			}
			if (Input.GetButtonDown("Short")){
				LoadByIndex (2);
			}
		}
	}

	private void SubmitName(string inputText){
		dataMan.participantID = inputText;
		Debug.Log("Participant ID is: " + inputText);
	}

	public void LoadByIndex(int sceneIndex){
		SceneManager.LoadScene (sceneIndex);
	}

	public void loadNextLevel(){
		Debug.Log ("last scene was " + dataMan.previousSceneIndex);
		if (3 <= dataMan.previousSceneIndex && dataMan.previousSceneIndex < 15) {
			int newSceneIndex = dataMan.previousSceneIndex + 1;
			SceneManager.LoadScene (newSceneIndex);
			Debug.Log ("now loading " + newSceneIndex);
		}
		if (dataMan.previousSceneIndex == 15) {
			SceneManager.LoadScene (18); //WinScreenFinal
		}
	}

	public void restartPreviousLevel(){
			Debug.Log ("last scene was " + dataMan.previousSceneIndex);
			SceneManager.LoadScene (dataMan.previousSceneIndex); 
	}

	public void clickSound(){
		FindObjectOfType<AudioManager> ().Play ("buttonPressed");
	}

	public void setExperimentGroup(int i){
		dataMan.experimentGroup = i;
		if (dataMan.participantID != "") {
			LoadByIndex (1);
		} else {
			Debug.Log ("WARNING: ID IS MISSING");
		}
	}
		
}
