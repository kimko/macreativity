﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aiEasy : MonoBehaviour {

	public float fpsTargetDistance;
	public float enemyLookDistance;
	public float attackDistance;
	public float enemyMovementSpeed;
	public float dampling;
	public Transform fpsTarget;
	Rigidbody theRigidbody;
	Renderer myRenderer;



	// Use this for initialization
	void Start () {
		myRenderer = GetComponent<Renderer> ();
		theRigidbody = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		enemyMove ();

		/*
		fpsTargetDistance = Vector3.Distance (fpsTarget.position, transform.position);
		if (fpsTargetDistance>enemyLookDistance){
			myRenderer.material.color = Color.green;
			lookAtPlayer ();
			print ("Look at Player Please");
		}

		if (fpsTargetDistance < attackDistance) {
			attackPlease ();
			print ("Attack");
		} else {
			myRenderer.material.color = Color.gray;
		
		}
		*/
			
	}

	void lookAtPlayer(){
		Quaternion rotation = Quaternion.LookRotation (fpsTarget.position - transform.position);
		transform.rotation = Quaternion.Slerp (transform.rotation, rotation, Time.deltaTime*dampling);
	}

	void attackPlease(){
		theRigidbody.AddForce (transform.forward*enemyMovementSpeed);
	}

	void enemyMove(){
		theRigidbody.AddForce (transform.forward*Random.Range(0.0f,1.0f));
	}
		
}
