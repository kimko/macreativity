﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Switch : MonoBehaviour {

	public bool viewIsGlobal; //true if global, false if local
	public float waitingTime; //time to wait after switching can be used again
	public bool switchable; //is switching allowed currently?
	public float blinkTime; //duration for which objects show their real shape
	public float blinkInterval; //time between each 'blink'

	// for exp group 2 (random)
	public float randomSwitchMaxFreq; //highest allowed frequency for random switching
	public float randomSwitchMinFreq; //lowest allowed frequency for random switching

	//lists of maze objects for switching
	GameObject[] treasures;
	GameObject[] enemies;
	GameObject[] shortcuts;
	//GameObject[] powerups; //cancelled
	// public GameObject deactivatedWalls; // brauchen wir nicht mehr

	//cameras
	Camera globalCam;
	Camera localCam;

	DataManager dataMan;

	//Animation
	private Animator anim;

	// Use this for initialization
	void Start () {

		//THESE VARIABLES CAN BE ADJUSTED
		waitingTime = 2.0f;
		randomSwitchMinFreq = 5.0f;
		randomSwitchMaxFreq = 12.0f;

		blinkTime = 0.5f;
		blinkInterval = 15.0f;

		//THESE VARIABLES SHOULDNT BE CHANGED
		globalCam = GameObject.Find ("GlobalCamera").GetComponent<Camera>();
		localCam = GameObject.Find("LocalCamera").GetComponent<Camera>();
		treasures = GameObject.FindGameObjectsWithTag("Objective");
		enemies = GameObject.FindGameObjectsWithTag("Enemy");
		shortcuts = GameObject.FindGameObjectsWithTag("Shortcut");
		//powerups = GameObject.FindGameObjectsWithTag("PowerUp");
		dataMan = FindObjectOfType<DataManager> ();

		InvokeRepeating ("blinkObjects", 2f, blinkInterval);

		switch (dataMan.experimentGroup) {
			// EXPGROUP1 = Autonomous Switching
			// start in global, switching allowed, switchbutton activated	
		case 1:
			randomizeStartView ();
			switchable = true;
			switchObjectAppearance ();
			anim = GameObject.Find("SwitchButton").GetComponent<Animator> ();
			break;
		// EXPGROUP2 = Forced Switching
		// start in global, switching allowed, switchbutton deactivated
		case 2:
			randomizeStartView ();
			switchable = true;
			switchObjectAppearance ();
			GameObject.Find("SwitchButton").gameObject.SetActive(false);
			break;
		// EXPGROUP3 = Only Local Mode
		// start in local, switching forbidden, switchbutton deactivated
		case 3:
			dataMan.playerStartingModePerLevel[dataMan.currentSceneIndex] = "LOCAL";
			viewIsGlobal = false;
			switchable = false;
			globalCam.enabled = false;
			localCam.enabled = true;
			switchObjectAppearance ();
			switchPlayerSize ();
			GameObject.Find("SwitchButton").gameObject.SetActive(false);
			break;
		// EXPGROUP4 = Only Global Mode
		// start in global, switching forbidden, switchbutton deactivated
		case 4: 
			dataMan.playerStartingModePerLevel[dataMan.currentSceneIndex] = "GLOBAL";
			viewIsGlobal = true;
			switchable = false;
			globalCam.enabled = true;
			localCam.enabled = false;
			switchObjectAppearance ();
			GameObject.Find ("SwitchButton").gameObject.SetActive (false);
			break;
		}
	}
	
	void Update () {
		// ONLY EXPGROUP1: press space to switch views 
		if (Input.GetButtonDown ("Switch") && switchable && dataMan.experimentGroup == 1) {
			switchViews ();
			anim.Play ("LoadingSwitch");
			StartCoroutine (waitAfterSwitch (waitingTime));
		} 
		// ONLY EXPGROUP2: randomly switch views
		else if (switchable && dataMan.experimentGroup == 2) {
			Debug.Log ("G2: RANDOM SWITCH");
			switchViews ();
			Random.InitState (System.DateTime.Now.Millisecond);
			waitingTime = Random.Range (randomSwitchMaxFreq, randomSwitchMinFreq);
			Debug.Log ("G2: switch again after " + waitingTime);
			StartCoroutine (waitAfterSwitch (waitingTime));
		} 
			
		//DEBUG: this is just to visualize the shortcuts in the editor while playtesting
		foreach (GameObject shortcut in shortcuts) {
			Debug.DrawRay(shortcut.transform.position + new Vector3(0,2.5f,0), 
				shortcut.transform.TransformDirection(Vector3.forward) * 2.0f, Color.magenta);
			Debug.DrawRay(shortcut.transform.position + new Vector3(0,2.5f,0), 
				shortcut.transform.TransformDirection(Vector3.back) * 2.0f, Color.magenta);
		}

		dataMan.playerDurationPerLevel[dataMan.currentSceneIndex] += Time.deltaTime;
		if (viewIsGlobal) {
			dataMan.playerDurationGlobalPerLevel [dataMan.currentSceneIndex] += Time.deltaTime;
		} else {
			dataMan.playerDurationLocalPerLevel [dataMan.currentSceneIndex] += Time.deltaTime;
		}
	}
		
	// randomly start either in local or in global (for groups 1 and 2)
	void randomizeStartView(){
		Random.InitState (System.DateTime.Now.Millisecond);
		int randomStartView = Random.Range (0, 2); 

		switch (randomStartView) {
		case 0:
			Debug.Log ("Random starting view is: GLOBAL");
			dataMan.playerStartingModePerLevel[dataMan.currentSceneIndex] = "GLOBAL";
			viewIsGlobal = true;
			globalCam.enabled = true;
			localCam.enabled = false;
			break;
		case 1:
			Debug.Log ("Random starting view is: LOCAL");
			dataMan.playerStartingModePerLevel[dataMan.currentSceneIndex] = "LOCAL";
			viewIsGlobal = false;
			globalCam.enabled = false;
			localCam.enabled = true;
			switchPlayerSize ();
			break;
		}
	}

	void switchViews(){
		dataMan.playerSwitchTimestampsPerLevel [dataMan.currentSceneIndex].Insert(dataMan.playerSwitchesPerLevel [dataMan.currentSceneIndex], System.DateTime.Now.ToString("HH:mm:ss"));
		//Debug.Log ("item at index " + dataMan.playerSwitchesPerLevel [dataMan.currentSceneIndex] + " is " + dataMan.playerSwitchTimestampsPerLevel [dataMan.currentSceneIndex][dataMan.playerSwitchesPerLevel [dataMan.currentSceneIndex]]);

		//dataMan.playerSwitchTimestampsPerLevel [dataMan.currentSceneIndex].Add (System.DateTime.Now.ToString ());
		//dataMan.playerSwitchTimestampsPerLevelList.Add(System.DateTime.Now.ToString ());
		dataMan.playerSwitchesPerLevel [dataMan.currentSceneIndex]++;

		if (viewIsGlobal) {
			globalCam.enabled = false;
			localCam.enabled = true;
			viewIsGlobal = false;
		}
		else if (!viewIsGlobal) {
			globalCam.enabled = true;
			localCam.enabled = false;
			viewIsGlobal = true;
		}
		switchObjectAppearance ();
		switchPlayerSize ();
		FindObjectOfType<AudioManager>().Play("switching");
	}

	// switch each enemy's and treasure's appearance to local (real) or global (gem)
	// deactivate powerups when in global mode
	void switchObjectAppearance(){
		foreach(GameObject treasure in treasures) {
			if (viewIsGlobal) {
				treasure.transform.Find("GlobalTreasure").gameObject.SetActive(true);
				treasure.transform.Find("LocalTreasure").gameObject.SetActive(false);
			}
			else if (!viewIsGlobal) {
				treasure.transform.Find("GlobalTreasure").gameObject.SetActive(false);
				treasure.transform.Find("LocalTreasure").gameObject.SetActive(true);
			}
		}

		foreach(GameObject enemy in enemies) {
			if (viewIsGlobal) {
				enemy.transform.Find("GlobalEnemy").gameObject.SetActive(true);
				enemy.transform.Find("LocalEnemy").gameObject.SetActive(false);
			}
			else if (!viewIsGlobal) {
				enemy.transform.Find("GlobalEnemy").gameObject.SetActive(false);
				enemy.transform.Find("LocalEnemy").gameObject.SetActive(true);
			}
		}
			
		/*
		foreach (GameObject powerup in powerups) {
			if (powerup != null) {
				if (viewIsGlobal) {
					powerup.gameObject.SetActive (false);
				} else if (!viewIsGlobal) {
					powerup.gameObject.SetActive (true);
				}
			}
		}
		*/
	}

	// player is smaller in local mode for better perspective 
	// box collider is enlarged to maintain about the same size
	void switchPlayerSize (){
		if (viewIsGlobal) {
			transform.localScale += new Vector3 (1.5F, 0, 1.5F);
			GetComponent<BoxCollider> ().size = new Vector3 (0.8f, 2, 0.8f);
		}
		else if (!viewIsGlobal) {
			transform.localScale += new Vector3 (-1.5F, 0, -1.5F);
			GetComponent<BoxCollider> ().size = new Vector3 (3, 1.8f, 3);
		}
	}

	// treasures & enemies switch to their real shape (to be distinguishable) ...
	void blinkObjects(){
		foreach (GameObject treasure in treasures) {
			treasure.transform.Find ("GlobalTreasure").gameObject.SetActive (false);
			treasure.transform.Find ("LocalTreasure").gameObject.SetActive (true);
		}
		foreach(GameObject enemy in enemies) {
			enemy.transform.Find("GlobalEnemy").gameObject.SetActive(false);
			enemy.transform.Find("LocalEnemy").gameObject.SetActive(true);
		}
		StartCoroutine("blinkBack");
	}

	// ...and back to the indistinguishable gem shape (only if currently in global view)
	IEnumerator blinkBack (){
		yield return new WaitForSeconds(blinkTime);
		if (viewIsGlobal) {
			foreach (GameObject treasure in treasures) {
				treasure.transform.Find ("GlobalTreasure").gameObject.SetActive (true);
				treasure.transform.Find ("LocalTreasure").gameObject.SetActive (false);
			}
			foreach (GameObject enemy in enemies) {
				enemy.transform.Find ("GlobalEnemy").gameObject.SetActive (true);
				enemy.transform.Find ("LocalEnemy").gameObject.SetActive (false);
			}
		}
	}

	// switching is deactivated for a certain time after use
	IEnumerator waitAfterSwitch (float waitingTime){
		switchable = false;
		yield return new WaitForSeconds(waitingTime);
		switchable = true;
	}

	/* // brauchen wir nicht mehr
	void switchToGlobalOnlyWalls(){
		GameObject.Find ("DeactivatedWalls").SetActive (true);
		deactivatedWalls.SetActive (true);
		foreach (GameObject shortcut in shortcuts) {
			shortcut.SetActive (false);
		}
	}
	*/

}
