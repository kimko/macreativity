﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour {

	public float[] timerList; //list of timer values for each level
	public float timer;
	float lastFullFloat;
	bool tenSecCountDownStarted;

	GameObject globalOutside;
	GameObject localOutside;
	Transform globalClouds;
	Transform localClouds;

	float skySteps; 
	float skyTime;

	float globalCloudSteps;
	float localCloudSteps;

	// Use this for initialization
	void Start () {
		globalOutside = GameObject.Find("GlobalOutside");
		localOutside = GameObject.Find("LocalOutside");
		globalClouds = globalOutside.transform.Find ("Clouds");
		localClouds = localOutside.transform.Find ("Clouds");

		// FALLS wir verschiedene timer pro gruppe wollen könnte man hier einfach ein switch break statement einfügen
		timerList = new float[SceneManager.sceneCountInBuildSettings];
		timerList [3] = 40f;	// Tutorial 1
		timerList [4] = 60f;	// Tutorial 2
		timerList [5] = 100f;	// Tutorial 3
		timerList [6] = 150f;	// Tutorial 4
		timerList [7] = 300f;	// Forest – Level 1
		timerList [8] = 300f;	// Forest – Level 2
		timerList [9] = 300f;	// Forest – Level 3
		timerList [10] = 300f;	// Desert – Level 1
		timerList [11] = 300f;	// Desert – Level 2
		timerList [12] = 300f;	// Desert – Level 3
		timerList [13] = 300f;	// Scifi – Level 1
		timerList [14] = 300f;	// Scifi – Level 2
		timerList [15] = 300f;	// Scifi – Level 3

		timer = timerList [SceneManager.GetActiveScene ().buildIndex];
		//timer = 300f; //NUR ZUM LEVELTESTEN DA DAMIT MAN NICHT STIRBT

		skySteps = 0.4f / timer;
		globalCloudSteps = 20f / timer;
		localCloudSteps = 20f / timer;
		skyTime = 0.0f;
		lastFullFloat = timer; 
	}
	
	// Update is called once per frame
	void Update () {
		// timer starts immediately
		timer -= Time.deltaTime;

		// in the last ten secs a countdown noise starts
		if (timer < 10.0f && !tenSecCountDownStarted) {
			FindObjectOfType<AudioManager>().Play("countdown");
			tenSecCountDownStarted = true;
		}

		// when timer is up, game over
		if (timer < 0) {
			Debug.Log ("Time is up! Game Over");
			GetComponent<PlayerBehaviour>().gameOver ();
		}

		// sky gets darker while timer goes down
		if(timer <= lastFullFloat-1f){
			skyTime += skySteps;
			localOutside.transform.Find("SkyDome").GetComponent<Renderer>().material.SetTextureOffset("_MainTex", new Vector2(skyTime, 0));
			globalOutside.transform.Find("SkyDome").GetComponent<Renderer>().material.SetTextureOffset("_MainTex", new Vector2(skyTime, 0));
			lastFullFloat = timer;
			globalClouds.transform.position = new Vector3 (
				globalClouds.transform.position.x,
				globalClouds.transform.position.y-globalCloudSteps, 
				globalClouds.transform.position.z);

			localClouds.transform.position = new Vector3 (
				localClouds.transform.position.x,
				localClouds.transform.position.y, 
				localClouds.transform.position.z-localCloudSteps);

			/* man könnte die sterne auch noch runter sinken lassen aber das siehtn bissl komisch aus!
			Transform localStars = localOutside.transform.Find ("SkyDome").transform.Find ("Stars");
			if (localStars.transform.position.y > 10) {
				localStars.transform.position = new Vector3 (
					localStars.transform.position.x,
					localStars.transform.position.y - localCloudSteps, 
					localStars.transform.position.z);
			}
			*/
		}
	}
}
