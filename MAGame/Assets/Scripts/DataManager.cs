﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DataManager : MonoBehaviour {

	public static DataManager instance;

	public string participantID;
	[Range(1,4)] // 1 = Autonomous, 2 = Forced, 3 = Local, 4 = Global
	public int experimentGroup;

	public int previousSceneIndex;
	public int currentSceneIndex;
	public string previousSceneName;
	public string currentSceneName;
	public int currentLevel;

	public string[] playerStartingModePerLevel;
	public int[] playerSwitchesPerLevel;
	public int[] playerShortcutsPerLevel;
	public int[] playerDeathsPerLevel;
	public int[] playerLivesLostPerLevel;
	public float[] playerDurationPerLevel;
	public float[] playerDurationLocalPerLevel;
	public float[] playerDurationGlobalPerLevel;
	public string[] playerStartingTimestampPerLevel;
	public string[] playerFinishingTimestampPerLevel;
	public List<string>[] playerSwitchTimestampsPerLevel = new List<string>[16];
	public List<string>[] playerShortcutTimestampsPerLevel = new List<string>[16];

	public List<string[]> playerData = new List<string[]>();

	// Use this for initialization
	void Awake () {
		DontDestroyOnLoad(this);
		currentSceneIndex = SceneManager.GetActiveScene ().buildIndex;
		currentSceneName = SceneManager.GetActiveScene ().name;
		SceneManager.activeSceneChanged += MyMethod;

		// making the header (first column) of the save file
		string[] saveDataTemp = new string[13];
		saveDataTemp[0] = "Level";
		saveDataTemp[1] = "Starting Mode";
		saveDataTemp[2] = "Switches";
		saveDataTemp[3] = "Shortcuts";
		saveDataTemp[4] = "Lives Lost";
		saveDataTemp[5] = "Deaths";
		saveDataTemp[6] = "Duration in Sec";
		saveDataTemp[7] = "Duration Local in Sec";
		saveDataTemp[8] = "Duration Global in Sec";
		saveDataTemp[9] = "Starting Timestamp";
		saveDataTemp[10] = "Finishing Timestamp";
		saveDataTemp[11] = "Switch Timestamps";
		saveDataTemp[12] = "Shortcut Timestamps";
		playerData.Add(saveDataTemp);
		fillListWithEmpty ();
		Debug.Log ("SAVING: MAKING HEADER ROW");
	}

	void Update(){

		if (currentSceneIndex != 0) {
			// CHEATS FÜR EXPERIMENT
			// 1 drücken um Player zur Startposition zu resetten
			if (Input.GetKeyDown ("1") && currentSceneIndex >= 3 && currentSceneIndex <= 15 ) {
				GameObject.Find ("Player").GetComponent<PlayerBehaviour> ().transform.position =
					GameObject.Find ("Player").GetComponent<PlayerBehaviour> ().startPosition;
			}
			// 2 drücken um Szene neu zu laden
			if (Input.GetKeyDown ("2")) {
				SceneManager.LoadScene (currentSceneIndex);
			}
			// 3 drücke um zur Levelauswahl zu kommen
			if (Input.GetKeyDown ("3")) {
				SceneManager.LoadScene (2);
			}
			// 4 drücke um zum Startbildschirm zu kommen
			if (Input.GetKeyDown ("4")) {
				SceneManager.LoadScene (1);
			}
		}

		savingPlayerData ();
		if (experimentGroup != 0) {
			GetComponent<SaveDataToCSV> ().saveToCSV (playerData);
		}
		//Debug.Log ("SAVING: DATA TO CSV FILE");

	}

	void MyMethod(Scene previousScene, Scene newScene)
	{
		previousSceneIndex = currentSceneIndex;
		currentSceneIndex = SceneManager.GetActiveScene ().buildIndex;

		previousSceneName = currentSceneName; 
		currentSceneName = SceneManager.GetActiveScene ().name;

		//savingPlayerData (previousSceneIndex);
	}

	void savingPlayerData(){
		for (int i = 1; i <= 13; i++) {
			
			string[] saveDataTemp = new string[13];
			saveDataTemp [0] = i.ToString ();
			saveDataTemp [1] = playerStartingModePerLevel [i+2].ToString ();
			saveDataTemp [2] = playerSwitchesPerLevel [i+2].ToString ();
			saveDataTemp[3] = playerShortcutsPerLevel[i+2].ToString();
			saveDataTemp[4] = playerLivesLostPerLevel[i+2].ToString();
			saveDataTemp[5] = playerDeathsPerLevel[i+2].ToString();
			saveDataTemp[6] = playerDurationPerLevel[i+2].ToString();
			saveDataTemp[7] = playerDurationLocalPerLevel[i+2].ToString();
			saveDataTemp[8] = playerDurationGlobalPerLevel[i+2].ToString();
			saveDataTemp[9] = playerStartingTimestampPerLevel[i+2].ToString();
			saveDataTemp[10] = playerFinishingTimestampPerLevel[i+2].ToString();
			saveDataTemp[11] = combineTimestamps(i+2, playerSwitchTimestampsPerLevel);
			saveDataTemp[12] = combineTimestamps(i+2, playerShortcutTimestampsPerLevel);
			//Debug.Log ("accessing index " + i + " of playerdata (max: " + playerData.Count + " and index " + (i + 2).ToString () + " of arrays (max: " + playerSwitchesPerLevel.Length);
			playerData [i] = saveDataTemp;
		}
	}

	// macht einen langen string aus liste von timestamps
	string combineTimestamps(int i, List<string>[] list){
		string temp = "";
		//Debug.Log ("individiual list count is " + list [i].Count);
		//Debug.Log("accessing item " + playerSwitchesPerLevel [i]);

		for (int j = 0; j < list[i].Count; j++) {
			if (j == 0) {
				temp = list [i] [j];
			} else {
				temp = temp + " || " + list [i] [j];
			}
		}
		return temp;
	}


	void fillListWithEmpty(){
		for (int i = 1; i <= 15; i++) {
			string[] saveDataTemp = new string[5];
			playerData.Add (saveDataTemp);
		}
		//Debug.Log ("playerdata size is " + playerData.Count);

		for (int j = 0; j <= 15 ; j++){
			playerSwitchTimestampsPerLevel[j] = new List<string>();
			playerShortcutTimestampsPerLevel[j] = new List<string>();
		}
	}
}
