﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour {

	Rigidbody rb;
	int randomNum;
	LayerMask obstacleMask;
	LayerMask playerMask;
	public float enemySpeed, localEnemySpeed, globalEnemySpeed;
	bool chasingPlayer; //true wenn player gesichtet wurde, beeinflusst movement verhalten
	float commitToDirection; //sorgt dafür, dass random abbiegen funktioniert

	PlayerMovement player;
	Switch playerview;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();

		obstacleMask = LayerMask.GetMask("Default", "Shortcut"); //objectives and player should be on layer: Ignore Raycast
		playerMask = LayerMask.GetMask("Player");

		Physics.IgnoreLayerCollision(0, 8); 
		localEnemySpeed = 2.5f;
		globalEnemySpeed = 5;
		commitToDirection = 0f;

		player = GameObject.Find ("Player").GetComponent<PlayerMovement>();
		playerview = GameObject.Find ("Player").GetComponent<Switch> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		enemyMove ();

		resetEnemySpeed (globalEnemySpeed, localEnemySpeed);

		// enemies will get faster and follow the player when they spot them
		if (gameObject.tag == "Enemy") {
			Vector3 fwd = transform.TransformDirection(Vector3.forward);
			Debug.DrawRay(transform.position, fwd * 20.0f, Color.cyan);
			if (isPlayerInLineOfSight (fwd)) {
				resetEnemySpeed (10, 6);
				player.localSpeed = 6;
				player.globalSpeed = 10;
				if (!chasingPlayer) {	FindObjectOfType<AudioManager> ().Play("enemySeesPlayer");	}
				chasingPlayer = true;
			}
			if (!isPlayerInLineOfSight (fwd) && !chasingPlayer) {
				resetEnemySpeed (globalEnemySpeed, localEnemySpeed);
				player.localSpeed = 3;
				player.globalSpeed = 8;
			}
		}

		// treasure will make a u-turn when they "spot" the player
		if (gameObject.tag == "Objective") {
			Vector3 fwd = transform.TransformDirection(Vector3.forward);
			Debug.DrawRay(transform.position, fwd * 20.0f, Color.blue);
			if (isPlayerInLineOfSight (fwd)) {
				transform.Rotate (0, 180, 0);
			}
		}
	}

	void resetEnemySpeed(float globalSpeed, float localSpeed){
		if (playerview.viewIsGlobal) {
			enemySpeed = globalSpeed;
		} else {
			enemySpeed = localSpeed;
		}
	}

	void enemyMove(){
		//directions to check for obstacles/player
		Vector3 fwd = transform.TransformDirection(Vector3.forward);
		Vector3 right = transform.TransformDirection(Vector3.right);
		Vector3 left = transform.TransformDirection(Vector3.left);

		//zum debuggen
		if (gameObject.tag == "Enemy") {
			Debug.DrawRay (transform.position- fwd, right * 2.0f, Color.green);
			Debug.DrawRay (transform.position- fwd, left * 2.0f, Color.green);
		} else if (gameObject.tag == "Objective") {
			Debug.DrawRay (transform.position- fwd, right * 2.0f, Color.red);
			Debug.DrawRay (transform.position- fwd, left * 2.0f, Color.red);		}

		/*
		 Debug.DrawRay (transform.position, fwd * 3.0f, Color.blue);
			Debug.DrawRay (transform.position - fwd, right * 4.0f, Color.red);
			Debug.DrawRay (transform.position, right * 4.0f, Color.red);
			Debug.DrawRay (transform.position - fwd, left * 4.0f, Color.green);
			Debug.DrawRay (transform.position, left * 4.0f, Color.green);
		*/

		//use current time as random seed
		Random.InitState (System.DateTime.Now.Millisecond);

		//wenn objekt auf wand stößt…
		if (Physics.Raycast (transform.position, fwd, 1.5f, obstacleMask.value)) {
			//…und player verfolgt, gucken in welche richtung player ist und sich in diese drehen
			if (chasingPlayer) {
				if(isPlayerInLineOfSight(transform.TransformDirection(Vector3.right))){
					transform.Rotate (0, 90, 0);
				}
				else if(isPlayerInLineOfSight(transform.TransformDirection(Vector3.left))){
					transform.Rotate (0, -90, 0);
				}
				else {
					chasingPlayer = false;
				}
			//…und nicht verfolgt, randomly nach links oder rechts drehen
			} else {
				randomNum = Random.Range (0, 2); 
				if (randomNum == 0) {
					transform.Rotate (0, 90, 0);
					//Debug.Log ("WALL IN FRONT: turning right");
				} else if (randomNum == 1) {
					transform.Rotate (0, -90, 0);
					//Debug.Log ("WALL IN FRONT: turning left");
				}
				commitToDirection = 100 / enemySpeed;
			}
		//wenn zur rechten von objekt ein gang abgeht…
		} else if (!(Physics.Raycast (transform.position, fwd, 3.0f, obstacleMask.value))
		          && !(Physics.Raycast (transform.position - fwd, right, 4.0f, obstacleMask.value))
		          && !(Physics.Raycast (transform.position, right, 4.0f, obstacleMask.value))
		          && commitToDirection == 0) {
			//…und player verfolgt wird, gucken ob player rechts ist & abbiegen, ansonsten weitergehen
			if (chasingPlayer) {
				if (isPlayerInLineOfSight (transform.TransformDirection (Vector3.right))) {
					transform.Rotate (0, 90, 0);
				} else {
					rb.position += transform.forward * enemySpeed * Time.deltaTime;
				}
			//…und player nicht verfolgt wird, mit 50% chance rechts abbiegen
			} else {
				randomNum = Random.Range (0, 2); 
				if (randomNum == 0) {
					transform.Rotate (0, 90, 0);
					//Debug.Log ("RIGHT FREE: turning right");
				} else if (randomNum == 1) {
					//Debug.Log ("RIGHT FREE: dont turn");
				}
				commitToDirection = 100 / enemySpeed;
			}
		//dasselbe mit links
		} else if (!(Physics.Raycast (transform.position, fwd, 3.0f, obstacleMask.value))
		          && !(Physics.Raycast (transform.position - fwd, left, 4.0f, obstacleMask.value))
		          && !(Physics.Raycast (transform.position, left, 4.0f, obstacleMask.value))
		          && commitToDirection == 0) {
			if (chasingPlayer) {
				if (isPlayerInLineOfSight (transform.TransformDirection (Vector3.left))) {
					transform.Rotate (0, -90, 0);
				} else {
					rb.position += transform.forward * enemySpeed * Time.deltaTime;
				}
			} else {
				randomNum = Random.Range (0, 2); 
				if (randomNum == 0) {
					transform.Rotate (0, -90, 0);
					//Debug.Log ("LEFT FREE: turning left");
				} else if (randomNum == 1) {
					//Debug.Log ("LEFT FREE: dont turn");
				}
				commitToDirection = 100 / enemySpeed;
			}
		}
		//wenn keiner der fälle eintritt, einfach weiter geradeaus gehen
		else {
			rb.position += transform.forward * enemySpeed * Time.deltaTime;
			//Debug.Log ("FRONT FREE & RIGHT/LEFT NOT: going forward");
			if (commitToDirection > 0) {
				commitToDirection--;
			}
		}
	}

	//gibt true wenn player in direkter sichtlinie des objekts ist
	bool isPlayerInLineOfSight(Vector3 rayDirection){
		//ist player vor mir?
		if (Physics.Raycast (transform.position, rayDirection, 20.0f, playerMask.value)) {
			//Debug.Log ("player in front of me (possibly obscured by obstacles)");
			float dstToTarget = Vector3.Distance (transform.position, GameObject.Find("Player").transform.position);
			//sind zwischen mir und player keine wände?
			if (!Physics.Raycast (transform.position, rayDirection, dstToTarget, obstacleMask.value)) {
				//Debug.Log ("PLAYER IN MY LINE OF SIGHT");
				return true;
			}
		}
		return false;
	}













}
